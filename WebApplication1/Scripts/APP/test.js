﻿$(function () {
    var oTable = new TableInit();
    oTable.Init();

    var oButtonInit = new ButtonInit();
    oButtonInit.Init();
});

var TableInit = function () {
    var oTableInit = new Object();
    //初始化Table
    oTableInit.Init = function () {
        $('#tab_result').bootstrapTable({
            //url: '',
            //method: 'post',                      //请求方式（*）
            //toolbar: '#toolbar',                //工具按钮用哪个容器
            striped: true,                      //是否显示行间隔色
            cache: false,                       //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
            pagination: true,                   //是否显示分页（*）
            sortable: false,                     //是否启用排序
            sortOrder: "asc",                   //排序方式
            //queryParams: oTableInit.queryParams,//传递参数（*）
            sidePagination: "server",           //分页方式：client客户端分页，server服务端分页（*）
            pageNumber: 1,                       //初始化加载第一页，默认第一页
            pageSize: 10,                       //每页的记录行数（*）
            pageList: [10, 25, 50, 100],        //可供选择的每页的行数（*）
            search: false,                       //是否显示表格搜索，此搜索是客户端搜索，不会进服务端，所以，个人感觉意义不大
            strictSearch: true,
            showColumns: false,                  //列下拉框
            showRefresh: false,                  //是否显示刷新按钮
            minimumCountColumns: 0,             //最少允许的列数
            clickToSelect: true,                //是否启用点击选中行
            height: 500,                        //行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
            uniqueId: "ID",                     //每一行的唯一标识，一般为主键列
            showToggle: false,                    //是否显示详细视图和列表视图的切换按钮
            showFullscreen: false,                //是否显示全屏按钮
            cardView: false,                    //是否显示详细视图
            detailView: false,                   //是否显示父子表
            columns: [
                //{checkbox: true },              //复选框
                { field: 'Input_a', title: '输入1' },             //获取控制器返回的数据
                { field: 'Input_b', title: '输入2' },
                { field: 'Result', title: '结果' },
            ],
            queryParams: function (params) {                   
                return {
                    limit: params.limit,   //页面大小
                    offset: params.offset,  //页码
                    Input_a: $('#input_a').val(),                    //传递数据给控制器
                    Input_b: $('#input_b').val(),
                };
            }
        });
    };
    return oTableInit;
};

var ButtonInit = function () {
    var oInit = new Object();
    oInit.Init = function () {
        $('#btn_cal').click(function () {
            $('#tab_result').bootstrapTable('refresh', { url: '/Plus/TabPlus' });
        });     
        $('#btn_lab').click(function () {
            getLabValue();      
        }); 
    };
    return oInit;
}

function getLabValue() {
    $.ajax({
        url: '/Plus/LabPlus',
        data: {
            input_a: $('#lab_a').val(),
            input_b: $('#lab_b').val(),
        },
        success: function (result) {
            document.getElementById('lab_result').innerText = result[0].Result;
        }
    })
}
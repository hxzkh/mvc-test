﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class HeadModel
    {
        public string UserName { get; set; }
        public string Time { get; set; }
    }

    public class DataModel
    {
        public int Input_a { get; set; }
        public int Input_b { get; set; }
        public int Result { get; set; }
    }
}
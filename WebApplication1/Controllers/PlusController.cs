﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;
using WebApplication1.ServiceReference1;

namespace WebApplication1.Controllers
{
    public class PlusController : Controller
    {
        // GET: Plus
        public ActionResult Index()
        {
            HeadModel head = new HeadModel { UserName = "Gao", Time = DateTime.Now.ToString() };
            ViewBag.head = head;
            ViewBag.result = "NULL";
            return View();
        }


        [HttpPost]
        public ActionResult Index(int? a, int? b)
        {
            HeadModel head = new HeadModel { UserName = "Gao", Time = DateTime.Now.ToString() };
            ViewBag.head = head;

            if (a != null && b != null)
            {
                Service1Client client = new Service1Client();
                string result = client.PlusNumber((int)a, (int)b);
                ViewBag.result = result;
            }
            else
                ViewBag.result = "NULL";
            return View();
        }

        public JsonResult TabPlus(int limit, int offset, int? input_a, int? input_b)
        {
            if (input_a != null && input_b != null)
            {
                Service1Client client = new Service1Client();
                string result = client.PlusNumber((int)input_a, (int)input_b);

                var data1 = new[]{
                    new DataModel { Input_a = (int)input_a, Input_b = (int)input_b, Result = int.Parse(result) },
                };

                //var list = new List<DataModel>();              
                //list.Add(data1);
                //var total = list.Count;
                //var rows = list.Skip(offset).Take(limit).ToList();
                //return Json(new { total = total, rows = rows }, JsonRequestBehavior.AllowGet);

                return Json(new { tatal = 1, rows = data1.ToList() }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { total = 0, rows = 0 }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LabPlus(int? input_a, int? input_b)
        {
            if (input_a != null && input_b != null)
            {
                Service1Client client = new Service1Client();
                string result = client.PlusNumber((int)input_a, (int)input_b);

                var data1 = new[]{
                    new DataModel { Input_a = (int)input_a, Input_b = (int)input_b, Result = int.Parse(result) },
                };
                return Json(data1.ToList(), JsonRequestBehavior.AllowGet);
            }
            else
            {
                var data1 = new[]{
                    new DataModel { Input_a = 0, Input_b = 0, Result = 0 },
                };
                return Json(data1.ToList(), JsonRequestBehavior.AllowGet);
            }              
        }
    }
}